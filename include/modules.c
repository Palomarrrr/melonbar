#include "modules.h"
#include "config.h"
#include "xcommon.h"
#include <X11/X.h>
#include <X11/Xft/Xft.h>
#include <X11/Xlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/utsname.h>

/*************************************
 *  Some built-in modules to keep
 *  basic functions from slowing
 *  things down
*************************************/

int L_OFFSET = 0;
int R_OFFSET = 0;

void PrintToBar(Display* dpy, Window* win, FontContext* fctx, GC* gc, char* msg, uint8_t style){
   // Get the string length 
    int msglen = strnlen(msg, 256);

    // Print the string
    XftTextExtentsUtf8(dpy, fctx->font, (XftChar8*)msg, msglen, &fctx->ext);

    if(style & STYLE_R_ALIGN){
        XftDrawStringUtf8(fctx->draw, &fctx->fontColor, fctx->font, R_OFFSET - fctx->ext.width, (user_cfg.bar_hgt + fctx->ext.y) / 2, (XftChar8 *)msg, msglen);
        XDrawLine(dpy, *win, *gc, R_OFFSET - fctx->ext.width - user_cfg.font_size, 3, R_OFFSET - fctx->ext.width - user_cfg.font_size, ((user_cfg.bar_hgt/2) + 3) * 2 - 8);
        R_OFFSET -= fctx->ext.width + (user_cfg.font_size * 2); // Set the offset for next module
    }else{ // Else just assume left align ig?
        XftDrawStringUtf8(fctx->draw, &fctx->fontColor, fctx->font, L_OFFSET, (user_cfg.bar_hgt + fctx->ext.y) / 2, (XftChar8 *)msg, msglen);
        XDrawLine(dpy, *win, *gc, L_OFFSET + fctx->ext.width + user_cfg.font_size, 3, L_OFFSET + fctx->ext.width + user_cfg.font_size, ((user_cfg.bar_hgt/2) + 3) * 2 - 8);
        L_OFFSET += fctx->ext.width + (user_cfg.font_size * 2); // Set the offset for next module
    }
}

// Get the current local time
// TODO - Add different formats ex. y-m-d, m-d-y, 24 hour time, etc
//          * prolly just default to `y-m-d` and 24 hr time since its a standard
void DisplayTime(Display* dpy, Window* win, FontContext* fctx, GC* gc, void* module){
    char* msg= malloc(sizeof(char) * 32);
    time_t t = time(NULL);
    struct tm* local_time= localtime(&t);
    char* ampm = "";

    // Check if we should format time in 12 hr
    if(((Module*)module)->module_params[0][0] == '1') {
        // Figure out whether it is AM or PM
        if(local_time->tm_hour >= 12){
            ampm = " PM";
            if(local_time->tm_hour >= 13) local_time->tm_hour -= 12;
        }else ampm = " AM";
    }

    // Check if we should format date as y-m-d 
    if(((Module*)module)->module_params[1][0] == '1')
        snprintf(msg, 32, "%02d-%02d-%04d | %02d:%02d:%02d%s", local_time->tm_mon+1, local_time->tm_mday, local_time->tm_year+1900, local_time->tm_hour, local_time->tm_min, local_time->tm_sec, ampm);
    else
        snprintf(msg, 32, "%04d-%02d-%02d | %02d:%02d:%02d%s", local_time->tm_year+1900, local_time->tm_mon+1, local_time->tm_mday, local_time->tm_hour, local_time->tm_min, local_time->tm_sec, ampm);

    PrintToBar(dpy, win, fctx, gc, msg, ((Module*)module)->style);

    free(msg);
}

// Display the current memory usage
// I don't this is working correctly either... Need to look into it
// TODO - Add a preprocessor switch for Linux and other OSs so that this can be used on BSDs
void DisplayMem(Display* dpy, Window* win, FontContext* fctx, GC* gc, void* module){
    static uint8_t err_status = 0;
    if(err_status) return;

    struct sysinfo info;
    sysinfo(&info);
    char* msg = malloc(sizeof(char) * 64);
    float max_mem = 0, used_mem = 0;
    int divisor = 1; // To get bytes to gigabytes
    uint8_t get_swap = ((Module*)module)->module_params[1][0] - '0'; // should basically do the same thing as above but ligher weight...

    // This should be done better... aka minimize heavy instructions like div | should be better...
    switch(((Module*)module)->module_params[0][0]){
        case 'G':
        case 'g':
            divisor *= 1000;
        case 'M':
        case 'm':
            divisor *= 1000;
        case 'K':
        case 'k':
            divisor *= 1000;
        case 'B':
        case 'b':
            break;
        default:{
            if(!(err_status & 0x1) && !get_swap){
                THROW_ERR("RAMUsage","An invalid memory unit was given to the module\nValid memory units include 'G', 'M', 'K', and 'B'");
                err_status |= 0x1;
            }else if(!(err_status & 0x2) && get_swap){
                THROW_ERR("SwapUsage","An invalid memory unit was given to the module\nValid memory units include 'G', 'M', 'K', and 'B'");
                err_status |= 0x2;
            }
        }
    }
    
    // These are in bytes so keep that in mind when dealing with this
    switch(get_swap){
        case 0:
            max_mem = (float)info.totalram / (float)divisor;
            used_mem = (float)(info.totalram - info.freeram) / (float)divisor;
            snprintf(msg, 64, "MEM:%.1f%cb/%.1f%cb", used_mem, ((Module*)module)->module_params[0][0], max_mem, ((Module*)module)->module_params[0][0]);
            break;
        default:
            max_mem = (float)info.totalswap / (float)divisor;
            used_mem = (float)(info.totalswap - info.freeswap) / (float)divisor;
            snprintf(msg, 64, "SWP:%.1f%cb/%.1f%cb", used_mem, ((Module*)module)->module_params[0][0], max_mem, ((Module*)module)->module_params[0][0]);
    }

    PrintToBar(dpy, win, fctx, gc, msg, ((Module*)module)->style);

    free(msg);
}

// Display battery percentage
// TODO: 
//      add in the ability to check other batteries (ie. BAT1 instead of only BAT0)
//      add the ability to check `capacity` and `capacity_designed`
void DisplayBattery(Display* dpy, Window* win, FontContext* fctx, GC* gc, void* module){
    static uint8_t err_status = 0; // This should act as a basic error flag
    FILE* fp;

    char* msg = malloc(sizeof(char) * 30);
    char* stat = malloc(sizeof(char) * 13);
    char* bat_path_chg_curr = malloc(sizeof(char) * 128);
    char* bat_path_chg_full = malloc(sizeof(char) * 128);
    char* bat_path_stat = malloc(sizeof(char) * 128);
    uint8_t bat_num = 0;
    int bat_now = 0;
    int bat_full = 0;
    uint8_t is_design = 0;

    if(((Module*)module)->module_params[1]) is_design = ((Module*)module)->module_params[1][0] - '0';
    else is_design = 0;
    // TODO: This is a bad and painfully slow solution
    if(((Module*)module)->module_params[0]) bat_num = atoi(((Module*)module)->module_params[0]);
    else bat_num = 0;

    // snprintf nightmare
    snprintf(bat_path_chg_curr, 128, "/sys/class/power_supply/BAT%u/charge_now", bat_num); 
    if(!is_design) snprintf(bat_path_chg_full, 128, "/sys/class/power_supply/BAT%u/charge_full", bat_num); 
    else snprintf(bat_path_chg_full, 128, "/sys/class/power_supply/BAT%u/charge_full_design", bat_num); 
    snprintf(bat_path_stat, 128, "/sys/class/power_supply/BAT%u/status", bat_num); 

    // Get the battery percentage
    fp = fopen(bat_path_chg_full, "r");

    if(!fp){ // If this file doesn't exist, the user might be on a desktop
        if(!err_status){ // Only print this once
            if(!is_design) THROW_ERR("DisplayBattery", "There was an error opening /sys/class/power_supply/BAT0/charge_full\nIf you're on a desktop, please disable this module");
            else THROW_ERR("DisplayBattery", "There was an error opening /sys/class/power_supply/BAT0/charge_full_design\nIf you're on a desktop, please disable this module");
            err_status = 1;
        }
        snprintf(msg, 64, "BAT%u: N/A",bat_num);
        goto battery_err;
    }else{
        fscanf(fp, "%d", &bat_full);
        fclose(fp);
    }

    if(!err_status) fp = fopen(bat_path_chg_curr, "r");

    if(!fp){ // If this file doesn't exist, the user might be on a desktop
        if(!err_status){ // Only print this once
            THROW_ERR("DisplayBattery", "There was an error opening /sys/class/power_supply/BAT0/charge_now\nIf you're on a desktop, please disable this module");
            err_status = 1;
        }
        snprintf(msg, 64, "BAT%u: N/A",bat_num);
        goto battery_err;
    }else{
        fscanf(fp, "%d", &bat_now);
        fclose(fp);
        // stupid slow and bad...
        bat_now = (int)(((float)bat_now / (float)bat_full) * 100.0);
    }


    // Get the battery status
    if(!err_status) fp = fopen(bat_path_stat, "r");

    if(!fp){ // If this file doesn't exist, the user might be on a desktop
        if(!err_status){ // Only print this once
            THROW_ERR("DisplayBattery", "There was an error opening /sys/class/power_supply/BAT0/status\nIf you're on a desktop, please disable this module");
            err_status = 1;
        }
        snprintf(msg, 64, "BAT%u: N/A",bat_num);
        goto battery_err;
    }else{
        fscanf(fp, "%s", stat);
        fclose(fp);
        snprintf(msg, 64, "BAT%u:%d%% (%s)", bat_num, bat_now, stat);
    }

battery_err:

    PrintToBar(dpy, win, fctx, gc, msg, ((Module*)module)->style);

    free(bat_path_chg_curr);
    free(bat_path_chg_full);
    free(bat_path_stat);
    free(stat);
    free(msg);
}

// Display CPU usage
// FIXME - I don't think this works properly... Need to look deeper into it
void DisplayCpu(Display* dpy, Window* win, FontContext* fctx, GC* gc, void* module){
    static uint8_t err_status = 0;
    if(err_status) return;
    FILE* fp;
    int n_cores; // number of cpu cores
    float curr_jobs = 0; // The actual cpu usage
    char* msg = malloc(sizeof(char) * 30);

    fp = fopen("/proc/cpuinfo", "r");

    if(fp == NULL){
        if(!err_status){
            THROW_ERR("CPUUsage", "Failed to read from /proc/cpuinfo\nYou may not have permission to read from this file");
            err_status = 1;
        }
        fclose(fp);
        return;
    }
    
    for(int i = 0; i < 10; i++){ // Skip to line 11
        fscanf(fp, "%*[^\n]\n");
        if(i == 9){
            fscanf(fp, "%*[^:]: %d", &n_cores);
        }
    }
    
    fclose(fp);

    fp = fopen("/proc/loadavg", "r");

    if(fp == NULL){
        if(!err_status){
            THROW_ERR("CPUUsage", "Failed to read from /proc/loadavg\nYou may not have permission to read from this file");
            err_status = 1;
        }
        fclose(fp);
        return;
    }
    
    fscanf(fp, "%f\n", &curr_jobs);
    fclose(fp);

    snprintf(msg, 29, "CPU: %4.1f%%", (curr_jobs * 100.0) / (float)n_cores); // Calculate the percentage and put it in the buffer
    
    PrintToBar(dpy, win, fctx, gc, msg, ((Module*)module)->style);

    free(msg);
}

// Display the current user@hostname
void DisplayUser(Display* dpy, Window* win, FontContext* fctx, GC* gc, void* module){
    static uint8_t has_run = 0;
    static char* users_name = NULL;
    static char* msg = NULL;
    static struct utsname uts = {0};

    if(!has_run){  // This doesn't really need to be ran more than once
        uname(&uts); // This fixes the problem where $HOSTNAME return NULL
        msg = malloc(sizeof(char) * 64);
        users_name = getenv("USER"); 
        snprintf(msg, 64, "%s@%s", users_name, uts.nodename);
        has_run = 1;
    }


    PrintToBar(dpy, win, fctx, gc, msg, ((Module*)module)->style);
}

// Display an environment variable
void DisplayEnvVar(Display* dpy, Window* win, FontContext* fctx, GC* gc, void* module){
    char* var_str = getenv(((Module*)module)->module_params[0]); // Thinking about caching this so it doesn't have to constantly run
    char* msg = malloc(sizeof(char) * 64);

    snprintf(msg, 64, ((Module*)module)->module_params[1], var_str);
    PrintToBar(dpy, win, fctx, gc, msg, ((Module*)module)->style);
    
    free(msg);
}

// Display info about the current kernel version
// TODO - Implement the format flag to allow the user to display whatever they want
void DisplayKernel(Display* dpy, Window* win, FontContext* fctx, GC* gc, void* module){
    static uint8_t has_run = 0;
    static char* msg = NULL;
    static struct utsname system_name = {0};

    if(!has_run){ // This doesn't need to run more than once
        uname(&system_name);
        msg = malloc(sizeof(char)*64);
        snprintf(msg, 64, "KRN: %s" , system_name.release);
        has_run = 1;
    }

    PrintToBar(dpy, win, fctx, gc, msg, ((Module*)module)->style);
}

void DisplayConnection(Display* dpy, Window* win, FontContext* fctx, GC* gc, void* module){
    static uint8_t err_status = 0;
    char* msg = calloc(64, sizeof(char));
    int status = 0;

    if(!(((Module*)module)->style & 0x1)){
        // This is really not a good thing...
        ((Module*)module)->module_params[3] = calloc(64, sizeof(char));
        snprintf(((Module*)module)->module_params[3],64, "/sys/class/net/%s/carrier", ((Module*)module)->module_params[0]); 
        ((Module*)module)->style |= 0x1;
    }

    FILE* fp = fopen(((Module*)module)->module_params[3], "r");

    if(!fp){ 
        if(!err_status){ // Only print this once
            THROW_ERR("DisplayConnection", "There was an error opening /sys/class/net/wlan0/carrier\nMake sure you actually have this adapter and that you typed its name correctly");
            err_status = 1;
        }
        snprintf(msg, 64, "NET: %s | N/A", ((Module*)module)->module_params[0]);
        goto _display_print;
    }else{
        fscanf(fp, "%d", &status);
        fclose(fp);
    }
    if(status) snprintf(msg, 64, "NET: %s | UP", ((Module*)module)->module_params[0]);
    else snprintf(msg, 64, "NET: %s | DOWN", ((Module*)module)->module_params[0]);

_display_print:
    PrintToBar(dpy, win, fctx, gc, msg, ((Module*)module)->style);

    free(msg);
}

void DisplayShellCMD(Display* dpy, Window* win, FontContext* fctx, GC* gc, void* module){
    static uint8_t err_status = 0;
    if(err_status) return;
    FILE* fp;
    char* msg = malloc(sizeof(char) * 64);
    fp = popen( ((Module*)module)->module_params[0], "r");

    if(fp == NULL){
        if(!err_status){
            THROW_ERR("DisplayShellCMD", "There was a problem running the provided command.\nMake sure to check your formatting");
            err_status = 1;
        }
        free(msg);
        return;
    }

    fgets(msg, 49, fp); // Grab the output of the command
    pclose(fp);

    msg[strnlen(msg,64) - 1] = '\0';

    PrintToBar(dpy, win, fctx, gc, msg, ((Module*)module)->style);

    free(msg);
}
